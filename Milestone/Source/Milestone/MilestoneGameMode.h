// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MilestoneGameMode.generated.h"

UCLASS(minimalapi)
class AMilestoneGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMilestoneGameMode();
};



