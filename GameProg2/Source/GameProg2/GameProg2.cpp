// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameProg2.h"
#include "Modules/ModuleManager.h"
DEFINE_LOG_CATEGORY(LabsLog);
IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GameProg2, "GameProg2" );
