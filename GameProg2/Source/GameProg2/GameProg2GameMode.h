// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GameProg2GameMode.generated.h"

UCLASS(minimalapi)
class AGameProg2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGameProg2GameMode();
};



